/*Date: 03/11/2022
  Name: Iana Feniuc
  NumberId: 2243911
*/
import java.util.Scanner;
public class TicTacToeGame
{
	public static void main(String[] args)
	{
		System.out.println("Welcome to Tik-Tak-Toe, HOHOHO !");
		//////////////////////////////////////////////////////
		Board game         = new Board();
		int player         = 1;
		Square playerToken = Square.X;
		boolean gameOver   = false;
		Scanner scan       = new Scanner(System.in);
		int moveNumber     = 1;
		////////////////////////////////////////////////////
		while(gameOver==false)
		{
		
		  System.out.println(game);
		//////////////////////////////
		  if(player==1)
		    playerToken=Square.X;
		  if(player==2)
			playerToken=Square.O;
		///////////////////////////////////////
		  System.out.println("----------------------------------"); 
		  System.out.println("Move "+moveNumber+"\n");
		  System.out.println("Player "+player+", it's your turn :\n");	 
		  System.out.println("Enter the row number");
		  int row      = scan.nextInt();
		  System.out.println("Enter the column number");
		  int column      = scan.nextInt();
		  System.out.println();
		//////////////////////////////////////////////////
		  while(game.placeToken(row,column,playerToken)==false)
		    {   
		        System.out.println("----------------------------------"); 
			    System.out.println("Re-enter valid values for your row and column please !!!");
			    System.out.println("Enter the row number");
			    row    = scan.nextInt();
			    System.out.println("Enter the column number");
			    column    = scan.nextInt();	
		    }
		////////////////////////////////////////////////////////////
		  if(game.checkIfWinning(playerToken))
		    {
			    System.out.println("Player "+player+" is the winner!");
			    gameOver=true;
		    }  
		   else if(game.checkIfFull())
		    {
			    System.out.println("It's a tie!");
			    gameOver=true;
		    }
		    else
		    {
			    player++;
			
			    if(player>2)
			        {
				        player=1;
			        }
		    }
		////////////////////////////////////////////////////////////////
		moveNumber++;
		}
	}
}