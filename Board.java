public class Board
{
	private Square[][] tictactoeBoard;
	
	//Constructor
	public Board()
	{
	    this.tictactoeBoard = new Square[3][3];
	    for(int i=0; i<this.tictactoeBoard.length;i++)
	      {
		    for(int j=0; j<this.tictactoeBoard[i].length;j++)
		        {
		           this.tictactoeBoard[i][j]=Square.BLANK;
		        }
	      }
	}
	
	//Print String
	public String toString()
	{
		String printer = "";
		for(int i=0; i<this.tictactoeBoard.length;i++)
	    {
		    for(int j=0; j<this.tictactoeBoard[i].length;j++)
		      {
		        printer+=this.tictactoeBoard[i][j]+" ";
		      }
		  printer+="\n";
	    }
	    return printer;
	}
	
	//Validate data
	public boolean placeToken(int row, int col,Square playerToken)
	{
		if(row<1 || row>3 || col<1 || col>3)
		{
			return false;
		}
		else if(this.tictactoeBoard[row-1][col-1]==Square.BLANK)
		{    
	        this.tictactoeBoard[row-1][col-1]=playerToken;
			return true;
		}
		else 
		{
			return false;
		}
	}
	//Check if the board game is full
	public boolean checkIfFull(){
		boolean value=true;
		for(int i=0; i<this.tictactoeBoard.length;i++)
	    {
		    for(int j=0; j<this.tictactoeBoard[i].length;j++)
		       {
		         if(this.tictactoeBoard[i][j]==Square.BLANK)
					 value = false;
		       }
	    }
		return value;
	} 
	
	//Check for an horizontal match !
	private boolean checkifWinningHorizontal(Square playerToken)
	{
		boolean value=false;
		
		for(int i=0; i<this.tictactoeBoard.length;i++)
	    { 
	       int numberOfTokenRow=0;
		    for(int j=0; j<this.tictactoeBoard[i].length;j++)
		        { 
		            if(this.tictactoeBoard[i][j]==playerToken)
				        {
					        numberOfTokenRow++;
				        }
		        }
			    if(numberOfTokenRow==3)
			        {
				        value=true;
			        }
	    }
		return value;
		
	}
	
	//Check for a vertical match !
	private boolean checkifWinningVertical(Square playerToken)
	{
		boolean value=false;
		for(int i=0; i<this.tictactoeBoard.length;i++)
	    { 
	       int numberOfTokenColumn=0;
		    for(int j=0; j<this.tictactoeBoard[i].length;j++)
		        { 
		            if(this.tictactoeBoard[j][i]==playerToken)
				        {
					        numberOfTokenColumn++;
				        }
		        }
			if(numberOfTokenColumn==3)
			    {
				  value=true;
			    }
	    }
		return value;
		
	}
	
	//Check which player is winning!
	public boolean checkIfWinning(Square playerToken)
	{
        if(this.checkifWinningHorizontal(playerToken) || this.checkifWinningVertical(playerToken))
			return true;
		else 
			return false;
	}
}